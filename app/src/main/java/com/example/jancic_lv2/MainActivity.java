package com.example.jancic_lv2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        ImageView imagePetrovic = (ImageView) findViewById(R.id.ivPetrovicSlika);
        ImageView imageAli = (ImageView) findViewById(R.id.ivAliSlika);
        ImageView imageKasparov = (ImageView) findViewById(R.id.ivKasparovSlika);

        imagePetrovic.setOnClickListener(this);
        imageAli.setOnClickListener(this);
        imageKasparov.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ImageView imagePetrovic = (ImageView) findViewById(R.id.ivPetrovicSlika);
        ImageView imageAli = (ImageView) findViewById(R.id.ivAliSlika);
        ImageView imageKasparov = (ImageView) findViewById(R.id.ivKasparovSlika);

        if(v.getId() == R.id.ivPetrovicSlika){
            imagePetrovic.setVisibility(View.INVISIBLE);
        }
        else if(v.getId() == R.id.ivAliSlika){
            imageAli.setVisibility((View.INVISIBLE));
        }
        else if (v.getId() == R.id.ivKasparovSlika){
            imageKasparov.setVisibility(View.INVISIBLE);
        }
    }

    public void addDescription(View v){
        RadioGroup rgRBs = findViewById(R.id.rgRBs);
        if(rgRBs.getCheckedRadioButtonId()==-1){
            Toast.makeText(getApplicationContext(), "Select a person", Toast.LENGTH_SHORT).show();
        }
        else{
            EditText editText = findViewById(R.id.etAddDescription);
            String description = editText.getText().toString();

            int selectedID = rgRBs.getCheckedRadioButtonId();
            RadioButton selectedRadioButton = findViewById(selectedID);

            if(selectedRadioButton.getId() == R.id.rbPetrovic){
                TextView petrovicDescription = findViewById(R.id.tvPetrovicDescription);
                petrovicDescription.setText(description);
            }
            if(selectedRadioButton.getId() == R.id.rbAli){
                TextView aliDescription = findViewById(R.id.tvAliDescription);
                aliDescription.setText(description);
            }
            if(selectedRadioButton.getId() == R.id.rbKasparov){
                TextView kasparovDescription = findViewById(R.id.tvKasparovDescription);
                kasparovDescription.setText(description);
            }
        }
    }
    public void inspirationalQuote(View v){
        Button btnInspiration = findViewById(R.id.btnInspirationalQuote);
        String arr[] = getResources().getStringArray(R.array.quotes_and_fun_facts);
        int rnd = new Random().nextInt(arr.length);
        Toast.makeText(this, arr[rnd], Toast.LENGTH_LONG).show();
    }
}